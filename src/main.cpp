#include <Arduino.h>

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <Time.h>
#include <string.h>
#include <sstream>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID "631be1aa-1e60-4bca-85cb-6c2768a49124"
#define OPEN_CHARACTERISTIC_UUID "14762daf-c9c6-40dd-8f3b-a3b9aa1fbbdf"
#define CLOSE_CHARACTERISTIC_UUID "f305f47c-8aba-4228-93e0-b173b63a05c4"
#define TIMESYNC_CHARACTERISTIC_UUID "fc1c00e3-e15c-4a3f-9f89-3ef9363b815c"
#define DIRECTUP_CHARACTERISTIC_UUID "ab19c32b-3c2c-4d96-bfb5-cf4eb8712c46"
#define DIRECTDOWN_CHARACTERISTIC_UUID "4d243c2a-b385-4c4c-8b85-d5b496f15b27"

struct TimePoint {
    int hour;
    int minute;
    int second;
};

BLECharacteristic *openCharacteristic;
BLECharacteristic *closeCharacteristic;
BLECharacteristic *timesyncCharacteristic;
BLECharacteristic *directupCharacteristic;
BLECharacteristic *directdownCharacteristic;

uint8_t directupActivated = 0;
uint8_t directdownActivated = 0;

TimePoint previousCheckedTime;
TimePoint openTime;
TimePoint closeTime;

bool justSynced = false;

// return -1 if the first timepoint happens first
// return 0 if both are equal
// return 1 if the second timepoint happens first

int cmpTimePoints(TimePoint a, TimePoint b) {
    if (a.hour < b.hour) {
        return -1;
    } else if (b.hour < a.hour) {
        return 1;
    } else {
        if (a.minute < b.minute) {
            return -1;
        } else if (b.minute < a.minute) {
            return 1;
        } else {
            if (a.second < b.second) {
                return -1;
            } else if (b.second < a.second) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}

void checkForAction() {
    if (!timeStatus()) {
        Serial.println("Actions are only executed if the time has been synced");
        return;
    }

    TimePoint currentTime;
    currentTime.hour = hour();
    currentTime.minute = minute();
    currentTime.second = second();

    // execute "open door" if the previous checked timepoint is before the set openTime
    // and the current timepoint is past openTime
    if (cmpTimePoints(previousCheckedTime, openTime) == -1 && cmpTimePoints(currentTime, openTime) >= 0) {
        Serial.println("open door");
    }

    if (cmpTimePoints(previousCheckedTime, closeTime) == -1 && cmpTimePoints(currentTime, closeTime) >= 0) {
        Serial.println("close door");
    }
}

TimePoint parseTime(std::string msg) {
    // received format: hh;mm;sec
    std::string delimiter = ";";
    std::string leftover;
    std::string hour;
    std::string minute;
    std::string second;
    size_t pos = 0;

    // find position of first delimiter
    pos = msg.find(delimiter);

    // extract a substring from 0 to the pos of the first delimiter
    hour = msg.substr(0, pos);

    // rest of the string is the minute and second value
    leftover = msg.substr(pos + 1);

    // extract minute and second the same way
    pos = leftover.find(delimiter);
    minute = leftover.substr(0, pos);
    second = leftover.substr(pos + 1);

    std::stringstream hourToInt(hour);
    std::stringstream minuteToInt(minute);
    std::stringstream secondToInt(second);

    TimePoint timePoint;
    hourToInt >> timePoint.hour;
    minuteToInt >> timePoint.minute;
    secondToInt >> timePoint.second;

    return timePoint;
}

void setup() {
    Serial.begin(115200);

    BLEDevice::init("esp32");
    BLEServer *pServer = BLEDevice::createServer();
    BLEService *pService = pServer->createService(SERVICE_UUID);

    // init characteristics
    openCharacteristic = pService->createCharacteristic(
        OPEN_CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    closeCharacteristic = pService->createCharacteristic(
        CLOSE_CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    timesyncCharacteristic = pService->createCharacteristic(
        TIMESYNC_CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    directupCharacteristic = pService->createCharacteristic(
        DIRECTUP_CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    directdownCharacteristic = pService->createCharacteristic(
        DIRECTDOWN_CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    openCharacteristic->setValue("8;0;0");
    closeCharacteristic->setValue("19;0;0");
    timesyncCharacteristic->setValue("-1;-1;-1");
    directupCharacteristic->setValue(&directupActivated, 1);
    directdownCharacteristic->setValue(&directdownActivated, 1);

    pService->start();
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
}

void loop() {
    std::string msg;

    // check if the time has been synced by the smartphone via ble and if so, sync the time of the esp32
    if (!timeStatus()) {
        msg = timesyncCharacteristic->getValue();
        TimePoint syncTime = parseTime(msg);
        if (syncTime.hour > 0) {
            setTime(syncTime.hour, syncTime.minute, syncTime.second, 0, 0, 2020);
            justSynced = true;
        }
    }

    // parse the current value from ble characteristic and update the timepoint values for open door and close door
    msg = openCharacteristic->getValue();
    openTime = parseTime(msg);

    msg = closeCharacteristic->getValue();
    closeTime = parseTime(msg);

    // check if the timepoint to open or close the door passed since the last wake up time
    // if so, perform the action to open or close the door
    // skip the checkForAction if the time was just synched the last wakeup, because then
    // the comparison between the timepoints leads to wrong actions
    if (justSynced) {
        justSynced = false;
    } else {
        checkForAction();
    }

    // check if the characteristics for direct control were set during the last sleep
    directupActivated = *directupCharacteristic->getData();
    if (directupActivated == 1) {
        Serial.println("open door");
        directupActivated = 0;
        directupCharacteristic->setValue(&directupActivated, 1);
    }

    directdownActivated = *directdownCharacteristic->getData();
    if (directdownActivated == 1) {
        Serial.println("close door");
        directdownActivated = 0;
        directdownCharacteristic->setValue(&directdownActivated, 1);
    }

    Serial.print(hour());
    Serial.print(":");
    Serial.print(minute());
    Serial.print(":");
    Serial.println(second());

    // sleep for 2 secondS
    previousCheckedTime.hour = hour();
    previousCheckedTime.minute = minute();
    previousCheckedTime.second = second();
    delay(2000);
}